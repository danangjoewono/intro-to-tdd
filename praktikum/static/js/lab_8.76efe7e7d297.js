window.fbAsyncInit = function() {
   FB.init({
     appId      : '1539422676141548',
     cookie     : true,
     xfbml      : true,
     version    : 'v2.11'
   });


     // implementasilah sebuah fungsi yang melakukan cek status login (getLoginStatus)
     // dan jalankanlah fungsi render di bawah, dengan parameter true jika
     // status login terkoneksi (connected)

     // Hal ini dilakukan agar ketika web dibuka dan ternyata sudah login, maka secara
     // otomatis akan ditampilkan view sudah login
     FB.getLoginStatus(function(response) {
         if (response.status === 'connected') {
           render(true);
         }
         else if (response.status === 'not_authorized') {
           render(false);
         }
         else {
           render(false);
         }
     });
   };

   // Call init facebook. default dari facebook
   (function(d, s, id) {
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) return;
     js = d.createElement(s); js.id = id;
     js.src = 'https://connect.facebook.net/en_US/sdk.js';
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

   // Fungsi Render, menerima parameter loginFlag yang menentukan apakah harus
   // merender atau membuat tampilan html untuk yang sudah login atau belum
   // Ubah metode ini seperlunya jika kalian perlu mengganti tampilan dengan memberi
   // Class-Class Bootstrap atau CSS yang anda implementasi sendiri
   const render = (loginFlag) => {
     if (loginFlag) {
       // Jika yang akan dirender adalah tampilan sudah login

       // Memanggil method getUserData (lihat ke bawah) yang Anda implementasi dengan fungsi callback
       // yang menerima object user sebagai parameter.
       // Object user ini merupakan object hasil response dari pemanggilan API Facebook.
       getUserData(user => {
         // Render tampilan profil, form input post, tombol post status, dan tombol logout
         $('#lab8').html(
             '<div class="profile container">' +
               '<div class="row">' +
                 '<div class="col-sm-12 col-md-12 col-12 col-lg-12" id="cover">' +
                   '<img class="cover" src="' + user.cover.source + '" alt="cover" />' +
                 '</div>' +
               '</div>' +
               '<div class="row">' +
                 '<div class="col-sm-4 col-md-4 col-4 col-lg-4" id="picture">' +
                   '<div class="square">' +
                     '<img class="picture" src="' + user.picture.data.url + '" alt="profpic" />' +
                   '</div>' +
                 '</div>' +
                 '<div class="col-sm-8 col-md-8 col-8 col-lg-8" id="name">' +
                   '<div class="name-section">' +
                     '<h1 class="name">' + user.name + '</h1>' +
                   '</div>' +
                 '</div>' +
               '</div>' +
               '<div class="row">'+
                 '<div class="col-sm-4 col-md-4 col-4 col-lg-4 about-me">' +
                   '<h3 class="about"><span class="glyphicon glyphicon-pencil"></span> ' + user.about + '</h3>' +
                   '<h3 class="email"><span class="glyphicon glyphicon-envelope"></span> ' + user.email + '</h3>' +
                   '<h3 class="gender"><span class="glyphicon glyphicon-user"></span> ' + user.gender + '</h3>' +
                 '</div>' +
                 '<div class="col-sm-6 col-md-6 col-6 col-lg-6 post-section">' +
                   '<div class="row">' +
                     '<h3 style="margin-left: 5%;"><strong>Post Status</strong></h3>' +
                   '</div>' +
                   '<div class="row">' +
                     '<input id="postInput" type="text" class="post" placeholder="Ketik Status Anda" />' +
                   '</div>' +
                   '<div class="row">' +
                     '<button class="post-status btn-primary" onclick="postStatus()">Post ke Facebook</button>' +
                   '</div>' +
                   '<div class="row">' +
                     '<div class="status">' +
                       '<h2 class="my-feed"><strong>My Feed</strong></h2>' +
                     '</div>' +
                   '</div>' +
                   '<div class="row">' +
                     '<button class="logout btn-danger" onclick="facebookLogout()">Logout</button>' +
                   '</div>' +
                 '</div>' +
               '</div>' +
             '</div>'
           );


         // Setelah merender tampilan di atas, dapatkan data home feed dari akun yang login
         // dengan memanggil method getUserPosts yang kalian implementasi sendiri.
         // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
         // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
         getUserFeed(feed => {
           feed.data.map(value => {
             // Render feed, kustomisasi sesuai kebutuhan.
             if (value.message && value.story) {
               console.log(value);
               $('.status').append(
                 '<div class="feed">' +
                   '<h1>' + value.message + '</h1>' +
                   '<h2>' + value.story + '</h2>' +
                   '<button class="btn btn-danger delete" onclick="deleteStatus(\''+ value.id + '\')">Delete</button>'+
                 '</div>'
               );
             } else if (value.message) {
               console.log(value);
               $('.status').append(
                 '<div class="feed">' +
                   '<h3>' + value.message + '</h3>' +
                   '<button class="btn btn-danger delete" onclick="deleteStatus(\''+ value.id + '\')">Delete</button>'+
                 '</div>'
               );
             } else if (value.story) {
               console.log(value);
               $('.status').append(
                 '<div class="feed">' +
                   '<h3>' + value.story + '</h3>' +
                   '<button class="btn btn-danger delete" onclick="deleteStatus(\''+ value.id + '\')">Delete</button>'+
                 '</div>'
               );
             }
           });
         });
       });
     } else {
       // Tampilan ketika belum login
       $('#lab8').html(
         '<h1 class="web-tittle" style="height: 75px;"><strong>My Lab 8</strong></h1>' +
         '<div class="container">' +
           '<div class="row jumbotron jumbotron1">' +
              '<div id="login-section">' +
                '<button class="btn btn-primary" onclick="facebookLogin()" id="login-button">Login with Facebook</button>' +
             '</div>' +
           '</div>' +
          '</div>'
       );
     }
   };

   function facebookLogin(){
     // TODO: Implement Method Ini
     // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan sudah login
     // ketika login sukses, serta juga fungsi ini memiliki segala permission yang dibutuhkan
     // pada scope yang ada. Anda dapat memodifikasi fungsi facebookLogin di atas.
     FB.login(function(response){
       if (response.status === "connected") {
         console.log(response);
         alert("Anda berhasil login menggunakan akun Facebook");
         render(true);
       } else {
         console.log("FL gagal");
         alert("Gagal login, coba lagi")
       }
     }, {scope:'public_profile,user_posts,publish_actions,user_about_me,email'})
   };

   function facebookLogout(){
     // TODO: Implement Method Ini
     // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan belum login
     // ketika logout sukses. Anda dapat memodifikasi fungsi facebookLogout di atas.
     FB.getLoginStatus(function(response) {
         if (response.status === 'connected') {
           FB.logout();
           render(false);
         }
     });
   };

   // TODO: Lengkapi Method Ini
   // Method ini memodifikasi method getUserData di atas yang menerima fungsi callback bernama fun
   // lalu merequest data user dari akun yang sedang login dengan semua fields yang dibutuhkan di
   // method render, dan memanggil fungsi callback tersebut setelah selesai melakukan request dan
   // meneruskan response yang didapat ke fungsi callback tersebut
   // Apakah yang dimaksud dengan fungsi callback?
   const getUserData = (fun) => {
     FB.getLoginStatus(function(response) {
         if (response.status === 'connected') {
           FB.api('/me?fields=id,name,about,email,gender,cover,picture.width(168).height(168)', 'GET', function(response){
             console.log(response);
             if (response && !response.error) {
               /* handle the result */
               picture = response.picture.data.url;
               name = response.name;
               fun(response);
             }
             else {
               alert("Something went wrong");
             }
           });
         }
     });
   };

   const getUserFeed = (fun) => {
     // TODO: Implement Method Ini
     // Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data Home Feed dari akun
     // yang sedang login dengan semua fields yang dibutuhkan di method render, dan memanggil fungsi callback
     // tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback
     // tersebut
     FB.getLoginStatus(function(response) {
         if (response.status === 'connected') {
           FB.api('/me/posts', 'GET', function(response){
             console.log(response);
             if (response && !response.error) {
               /* handle the result */
               fun(response);
             }
             else {
               alert("Something went wrong");
             }
           });
         }
     });
   };

   const postFeed = (message) => {
     // Todo: Implement method ini,
     // Pastikan method ini menerima parameter berupa string message dan melakukan Request POST ke Feed
     // Melalui API Facebook dengan message yang diterima dari parameter.
      FB.api('/me/feed', 'POST', {message:message});
      render(true);
   };

   const postStatus = () => {
     const message = $('#postInput').val();
     $('#postInput').val("");
     postFeed(message);
   };

   var picture, name;

   const deleteStatus = (id) => {
       var postId = id;
       FB.api(postId, 'delete', function(response) {
         console.log(response);
         if (!response || response.error) {
           alert('Error occured');
         } else {
           alert('Post was deleted');
           render(false);
           render(true);
         }
       });
   };
