storage = [
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
]
var backgroundColor;
var textColor;
var index;
//fungsi ganti tema
function changeTheme(bg, color){
  $("body").css({"backgroundColor": bg});
  $("h1").css({"color": color});
}
//Implementasi pergantian tema
if (localStorage.getItem("index") === null){
  index = 3;
}else{
  index = JSON.parse(localStorage.getItem("index"));
}
backgroundColor = storage[index]["bcgColor"];
textColor = storage[index]["fontColor"];
console.log(storage[index]["bcgColor"]);
changeTheme(backgroundColor, textColor);

$(document).ready(function(){
  $("#my-select").select2({
    "data" : storage
  });
  $("#my-select").val(index).change();
  $("#button").on("click", function(){  // sesuaikan class button
      // [TODO] ambil value dari elemen select #my-select
      index = $("#my-select").val();
      // [TODO] cocokan ID theme yang dipilih dengan daftar theme yang ada
      backgroundColor = storage[index]["bcgColor"];
      textColor = storage[index]["fontColor"];
      // [TODO] ambil object theme yang dipilih
      // [TODO] aplikasikan perubahan ke seluruh elemen HTML yang perlu diubah warnanya
      changeTheme(backgroundColor, textColor);
      // [TODO] simpan object theme tadi ke local storage selectedTheme
      localStorage.setItem("index", JSON.stringify(index));
  })
});

//calculator
var print = document.getElementById('print');
	var erase = false;

	var go = function(x) {
  if (x === 'ac') {
    print.value = '';
    erase = false;
  } else if (x === 'eval') {
    print.value = Math.round(evil(print.value)*10000)/10000;
    erase = true;
  } else {
    print.value += x;
  }
}

function evil(fn) {
  return new Function('return ' + fn)();
}

//chat box
$(document).ready(function(){
  $("#hideButton").click(function(){
      $(".chat-body").toggle(500);
  });
});
$(document).ready(function() {
$('textarea').keypress(function(event) {
		var $this = $(this);

		if(event.keyCode == 13){
			var msg = $this.val();
			$this.val('');
			$('.msg-insert').prepend("<div class='msg-send'>"+msg+"</div>");
			}
	});
});

// END
